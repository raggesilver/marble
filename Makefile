.PHONY: all export install run clean fclean update hard-update test-run \
	ffclean test

MODULE := marble
APP_ID := com.raggesilver.Marble
MANIFEST := $(APP_ID).json

APP_DIR := app
BUILD_DIR := app_build
REPO_DIR := repo

BUILD_COMMAND := meson --prefix=/app $(BUILD_DIR)
INSTALL_COMMAND := ninja -C $(BUILD_DIR) install

# Running `make` will compile libmarble alone, which sets up app_build with
# $(BUILD_COMMAND). Running `make test` will make libmarble and also build it's
# test files, which requires setting up app_build again with -Dtests=true.
# $(TESTL) and $(BUILDL) are just two mock files used to know whether the last
# build was normal or a test build. Basically if one exists it indicates that
# was the last build type. I.e. if $(TESTL) exists, the latest build was a test
# build, so app_build won't be reconfigured. If it doesn't exist and you run
# `make test` this Makefile will `clean` before continuing, meaning any standard
# build will get erased.

TESTL := /tmp/a2db54691b025da102947a54fd50b258fced733a5a76ed29f5f4febb33faab7d
BUILDL := /tmp/26c0b68d4c11c2c97d2d6000fc198d3ae6bf19934c0090c935fcb61f7dab4597

all: $(BUILDL) $(MODULE)

$(MODULE): $(BUILD_DIR) Makefile
	@echo "::Building -- $@"
	flatpak-builder --run $(APP_DIR) $(MANIFEST) $(INSTALL_COMMAND)

# app_build/
$(BUILD_DIR): $(APP_DIR) Makefile
	@echo "::Building -- $@"
	flatpak-builder --run $(APP_DIR) $(MANIFEST) $(BUILD_COMMAND)

# app/
$(APP_DIR):
	@echo "::Building -- $@"
	flatpak-builder --disable-updates --stop-at=$(MODULE) $@ $(MANIFEST)

# test lock
$(TESTL):
	@$(MAKE) --no-print-directory clean
	@echo "::Starting a test build"
	@touch $@

# build lock
$(BUILDL):
	@$(MAKE) --no-print-directory clean
	@echo "::Starting a regular build"
	@touch $@

# update dependencies, use cache if unchanged
update:
	@echo "::Updating depenndencies"
	flatpak-builder --ccache --force-clean --stop-at=$(MODULE) $(APP_DIR) $(MANIFEST)

# update all dependencies without cache (will rebuild everything) even if there
# is nothing new
hard-update:
	@echo "::Hard updating depenndencies"
	flatpak-builder --disable-cache --force-clean --stop-at=$(MODULE) $(APP_DIR) $(MANIFEST)

# generate $(REPO_DIR) and $(MODULE).flatpak
# export: $(MODULE)
# 	@echo "::Exporting"
# 	@echo "::Finishing -- $(MODULE)"
# 	flatpak-builder --finish-only $(APP_DIR) $(MANIFEST)
# 	@echo "::Exporting -- $(MANIFEST)"
# 	flatpak-builder --export-only --repo=$(REPO_DIR) $(APP_DIR) $(MANIFEST)
# 	@echo "::Building bundle -- $(MODULE).flatpak"
# 	flatpak build-bundle $(REPO_DIR) "$(MODULE).flatpak" $(APP_ID)

# install $(MODULE).flatpak
# install: export
# 	@echo "::Installing -- $(MODULE).flatpak"
# 	flatpak install --user "$(MODULE).flatpak"

# remove $(BUILD_DIR), $(REPO_DIR) and $(MODULE).flatpak
clean:
	@echo "::Cleaning previous build"
	@rm -rf $(BUILD_DIR)
	@rm -rf $(REPO_DIR)
	@rm -rf $(MODULE).flatpak
	@rm -f $(BUILDL)
	@rm -f $(TESTL)

# remove $(BUILD_DIR), $(REPO_DIR), $(MODULE).flatpak and $(APP_DIR)
fclean: clean
	@echo "::Cleaning flatpak dependencies"
	@rm -rf $(APP_DIR)

# remove everything from fclean plus .flatpak-builder
ffclean: fclean
	@echo "::Cleaning flatpak-builder's cache"
	@rm -rf .flatpak-builder

test: BUILD_COMMAND := meson --prefix=/app -Dtests=true $(BUILD_DIR)
test: $(TESTL)
test: $(MODULE)

# build (if necessary) and run $(MODULE) for 5 seconds
test-run: test
	flatpak-builder --run $(APP_DIR) $(MANIFEST) 'marble_test'

# some tests for this file
# make ffclean test-run export && make ffclean export
# make ffclean hard-update export
# make test-run; make update export

# keep this as reference:
# https://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html
