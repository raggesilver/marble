/* Loadable.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Loadable is a {@link Gtk.Overlay} that allows you to quickly toggle an
 * overlayed {@link Gtk.Spinner} on top of it's content. Useful when you run
 * async operations and need to let the user know something is being done in the
 * background.
 */
public class PQMarble.Loadable : Gtk.Overlay
{
  /**
   * Whether or not the widget is working. ``False`` by default.
   */
  public bool      loading { get; set; default = false; }

  private Gtk.Spinner  spinner;

  private const string DEFAULT_STYLE = """
    .loadable > spinner {
      background: alpha(darker(@theme_bg_color), .6);
    }
  """;

  /**
   * Creates a new {@link Marble.Loadable}.
   */
  public Loadable()
  {
    this.spinner = new Gtk.Spinner();
    this.spinner.start();

    this.add_overlay(this.spinner);
    this.set_overlay_pass_through(this.spinner, false);

    this.notify["loading"].connect(this.loading_changed);

    this.get_style_context().add_class("loadable");
    PQMarble.set_theming_for_data(this.spinner, DEFAULT_STYLE, null,
      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
  }

  private void loading_changed()
  {
    if (this.loading)
      this.spinner.show();
    else
      this.spinner.hide();
  }
}
