/* SideWindow.vala
 *
 * Copyright 2020 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Marble.SideWindow : Gtk.ApplicationWindow
{
    public Gtk.HeaderBar headerbar;
    public Gtk.HeaderBar side_headerbar;
    public Gtk.Box side_box;
    public Gtk.Box content_box;

    public bool dark_mode { get; set; default = true; }
    public bool show_window_controls { get; set; default = true; }
    public new string title { get; set; }

    private Gtk.SizeGroup size_group;

    construct
    {
        this.headerbar = new Gtk.HeaderBar();
        this.headerbar.get_style_context().add_class("main-headerbar");
        this.side_headerbar = new Gtk.HeaderBar();
        this.side_headerbar.get_style_context().add_class("side-headerbar");

        var b = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);

        b.pack_start(this.side_headerbar, false, true, 0);
        b.pack_start(this.headerbar, true, true, 0);

        this.set_titlebar(b);
        b.show_all();

        this.side_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        this.side_box.get_style_context().add_class("side-box");
        this.content_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        b = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        b.pack_start(this.side_box, false, true, 0);
        b.pack_start(this.content_box, true, true, 0);

        this.size_group = new Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL);
        this.size_group.add_widget(this.side_headerbar);
        this.size_group.add_widget(this.side_box);

        this.add(b);
        this.show_all();
    }

    public SideWindow(Gtk.Application app)
    {
        Object(application: app);

        var css_provider = new Gtk.CssProvider();
        css_provider.load_from_resource(
            "/com/raggesilver/Marble/resources/sidewindow.css");

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        this.connect_signals();
    }

    private void connect_signals()
    {
        var settings = Gtk.Settings.get_default();

        settings.gtk_application_prefer_dark_theme = this.dark_mode;
        this.update_headerbar_controls();

        settings.notify["gtk-decoration-layout"].connect(this.on_notify);
        this.notify.connect(this.on_notify);
    }

    private void on_notify(ParamSpec s)
    {
        var settings = Gtk.Settings.get_default();

        switch (s.name)
        {
            case "dark-mode":
            {
                settings.gtk_application_prefer_dark_theme = this.dark_mode;
                break;
            }
            case "gtk-decoration-layout":
            case "show-window-controls":
            {
                this.update_headerbar_controls();
                break;
            }
            case "title":
            {
                this.headerbar.set_title(this.title);
                break;
            }
            default: return;
        }
    }

    private void update_headerbar_controls()
    {
        string layout = Gtk.Settings.get_default().gtk_decoration_layout;
        bool is_on_left = (layout.contains(":appmenu") || layout.has_suffix(":"));

        this.side_headerbar.show_close_button = is_on_left;
        this.headerbar.show_close_button = !is_on_left;
    }
}
