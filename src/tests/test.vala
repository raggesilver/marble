/* SideWindowTest.vala
 *
 * Copyright 2020 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

[GtkTemplate (ui="/com/raggesilver/Marble/tests/layouts/marbletest.ui")]
class TestWindow : Gtk.ApplicationWindow {
    public TestWindow (Gtk.Application app) {
        Object(application: app);
    }

    [GtkCallback]
    private void on_run_side_window() {
        var win = new Marble.SideWindow(this.application);
        win.title = "SideWindow Test";

        var btn = new Gtk.Button.with_label("Click me");
        int i = 0;

        btn.clicked.connect(() => { win.title = @"CLICKED $i"; ++i; });
        btn.halign = Gtk.Align.CENTER;

        //  win.content_box.pack_start(btn, false, true, 0);
        win.content_box.set_center_widget(btn);

        var b = new Gtk.Button.from_icon_name("open-menu-symbolic",
                                            Gtk.IconSize.MENU);
        win.headerbar.pack_end(b);

        b = new Gtk.Button.from_icon_name("media-playback-start-symbolic",
                                        Gtk.IconSize.MENU);
        win.side_headerbar.pack_end(b);

        var lbl = new Gtk.Label("This is a\nside GtkBox");
        win.side_box.set_center_widget(lbl);

        win.show_all();

        win.set_size_request(800, 400);
    }
}

int main(string[] args)
{
    var app = new Gtk.Application("com.raggesilver.Marble.Test",
                                ApplicationFlags.FLAGS_NONE);

    app.activate.connect(() => {
        var win = new TestWindow(app);
        win.present();
    });

    return (app.run(args));
}
