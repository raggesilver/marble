<div align="center">
    <h1>
        <img src="https://gitlab.com/raggesilver/marble/raw/master/resources/com.raggesilver.Marble.svg" /> Marble
    </h1>
    <h4>My GTK library</h4>
    <p>
        <a href="https://gitlab.com/raggesilver/marble/pipelines">
            <img src="https://gitlab.com/raggesilver/marble/badges/master/pipeline.svg" alt="Build Status" />
        </a>
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="Marble on Patreon" />
        </a>
    </p>
    <p>
        <a href="#install">Install</a> •
        <a href="#features">Features</a> •
        <a href="https://gitlab.com/raggesilver/marble/blob/master/COPYING">License</a>
    </p>
</div>

Just as Elementary has Granite I have Marble, my collection of useful functions
and reusable widgets.

# Install

```bash
git clone https://gitlab.com/raggesilver/marble
cd marble
meson _build --prefix=/usr
ninja -C _build
sudo ninja -C _build install
```

If you ever want to remove it just
```bash
sudo ninja -C _build uninstall
```

**Dependencies**

- `meson` (>= 0.50.0) | `sudo dnf install meson` on Fedora

# Features

## `SideWindow`

`SideWindow` is a `Gtk.ApplicationWindow` implementation that divides it's
layout in "two" boxes, making it a pretty application window with space for a
side menu (such as GNOME Settings, Tweaks, etc).

> This is a screenshot of a test window shipped with Marble

![SideWindow preview](https://imgur.com/qZGwSxk.png)

# Todo

- Write docs
- Create examples

# Credits

Code derived from other sources is properly attributed on each file.
